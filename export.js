var util = require('util');
var colors = require('colors');
var fs = require('fs');
var path = require('path');
var xmldoc = require('xmldoc');

logger = require('./logger');

/*

 Usage:
  "node ."
  "node . pts" (see config.json)
  "node . C:\path\to\your\game"

 */

var fileExtensions = [
  ".xml"
];

var logger;
var config;

var setup = function(callback){


  var configDefaults = {
    default: 'prod',
    paths: {
      prod: 'C:\\Program Files\\Red 5 Studios\\Firefall',
      pts: 'C:\\Program Files\\Red 5 Studios\\Firefall_Public_Test'
    },
    logLevel: "info",
    runSync : false
  };


  logger.debugLevel = configDefaults.logLevel;

  try{

    var file = fs.openSync('./config.json', 'r');
    fs.close(file);
    config = require('./config.json');

    logger.debugLevel = config.logLevel;
    config.gamePath = config.paths[process.argv[2]] || process.argv[2] || config.paths[config.default];
    config.path = process.argv[2] || config.default;

    callback();

  }catch(e){
    fs.writeFileSync('./config.json', JSON.stringify(configDefaults, null, 2));
    logger.warn('No config file found. Default file has been created');
    logger.warn('Edit config.json and restart');
  }

};



var traverseFileSystem = function (currentPath, list) {
  
  list = (typeof list === "undefined") ? [] : list;

  var files = fs.readdirSync(currentPath);
  for (var i in files) {
     var currentFile = currentPath + '/' + files[i];
     var stats = fs.statSync(currentFile);
     if (stats.isFile()) {
      if(fileExtensions.indexOf(path.extname(currentFile)) > -1){
         list.push(currentFile)
      }
    }
    else if (stats.isDirectory()) {
       list = traverseFileSystem(currentFile, list);
     }
    
   }
   return list
};
 
var buildEventObject = function(collection, callback) {
  var inserted = 0;
  var events = {};
  
  var regEx = /<!--[\s\S]*?-->/g;
  
  for(var i = 0; i < collection.length; i++) {
    
    
    if(config.runSync){
      
        console.log("parsing " + collection[i]);
        var data = fs.readFileSync(collection[i], 'utf8');
        data = data.replace(regEx, "");
        var document = new xmldoc.XmlDocument(data);
        if(document.childNamed("Events")){
          document.childNamed("Events").children.forEach(function(e){
            events[e.attr.name] = e.attr.name
          })
        }
        
        if (++inserted == collection.length) {
          callback(null, events);
        }
      
      
    }else{
      
        fs.readFile(collection[i], 'utf8', function(err, data) {
          if (err) {
            callback(err);
            return;
          }
          data = data.replace(regEx, "");
          var document = new xmldoc.XmlDocument(data);
          if(document.childNamed("Events")){
            document.childNamed("Events").children.forEach(function(e){
              events[e.attr.name] = e.attr.name
            })
          }
          
          if (++inserted == collection.length) {
            callback(err, events);
          }
        });
      
    }
    
  }
};


var getGameVersion = function(cb){
  var version = '';
  var spawn = require('child_process').spawn,child;
  child = spawn('powershell.exe',['(Get-Item "' +config.gamePath + '\\system\\bin\\FirefallClient.exe").VersionInfo.PRODUCTVERSION']);
  child.stdout.on('data',function(data){
    version = data.toString().replace(/, /g, '.');
    version = version.replace(/[\r\n]/g, '')
  });
  child.stderr.on('data',function(data){
    console.log(('Powershell Errors: ' + data).red);
  });
  child.on('exit',function(){
    cb(version)
  });
  child.stdin.end(); //end input
};

setup(function(){

  getGameVersion(function(version){
    buildEventObject(traverseFileSystem(config.gamePath + "\\system\\gui"), function(err, events){
      if (err) {
        return console.log(err.toString.red);
      }
      var gfl = Object.keys(events).map(function(x){return x}).length;
      console.log("found ".yellow + gfl.toString().cyan + " in the game files".yellow);

      fs.readFile(config.gamePath + "\\system\\bin\\FirefallClient.exe", 'utf8', function (err,data) {
        if (err) {
          return console.log(err.toString.red);
        }

        var onEvents = /\x00(ON_.+?)\x00/gm;

        var result;
        while( result = onEvents.exec(data) ){
          events[result[1]] = result[1]
        }

        var evt = Object.keys(events).map(function(x){return x});

        console.log("found +".yellow + (evt.length - gfl).toString().cyan + " in the .exe".yellow);

        evt.sort();

        if(!fs.existsSync("events")){
           fs.mkdirSync("events", 0766, function(err){
             if(err){
               logger.error(err.toString.red);
               logger.error("ERROR! Can't create events directory! \n".red);
             }
           });
         }

        var fileContents = "# Event Export for \r\n" +
          "# " + config.path + "-" + version;

        fileContents += '\r\n\r\n' + evt.join('\r\n');

        fs.writeFile("events\\"+"events.txt", fileContents, 'utf8', function (err) {
           if (err) return console.log(err.toString.red);
           console.log("successfully wrote ".green + evt.length.toString().cyan + (" events from " + config.path + "-" + version).green)
        });

      });
  })
  });

});