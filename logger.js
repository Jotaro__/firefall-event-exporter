var logger = exports;
var colors = require('colors/safe')

logger.replace = false;
logger.debugLevel = 'warn';

logger.levels = ['error', 'warn', 'info', 'log'];

logger.last = 0;

logger.print = function(level, message) {
  var color = [colors.red, colors.yellow, colors.cyan, colors.gray]
  if (logger.levels.indexOf(level) <= logger.levels.indexOf(logger.debugLevel) ) {

    if (typeof message !== 'string') {
      message = JSON.stringify(message);
    };

    var msg = level+': '+message
    var msgout = color[logger.levels.indexOf(level)](msg)

    if(logger.replace){
      var w = "";
      for(var i = 1; i <= logger.last; i++){ w += " "};
      process.stdout.write(w + "\033[0G");
      process.stdout.write(msgout + "\033[0G");
    }else{
      console.log(msgout);
    }
    logger.last = msg.length
  }
}

logger.levels.forEach(function(e, i){
  logger[e] = function(message){
    logger.print(e, message)
  }
})
